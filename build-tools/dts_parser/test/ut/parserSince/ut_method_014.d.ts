/**
 * the ut for method in namespace, which has params and return value, one of params is optional.
 */
export namespace test {
  function testFunction(param1: string, param2?: string): number;
}