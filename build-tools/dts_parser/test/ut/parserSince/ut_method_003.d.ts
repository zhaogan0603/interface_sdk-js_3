/**
 * the ut for method in sourcefile, which has params and return value
 */
export declare function test(param1: string): number;