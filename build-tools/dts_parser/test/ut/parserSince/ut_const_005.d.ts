/**
 * the ut for const in namespace, has the tag of constant and initializer but no type
 */
export namespace test {
  /**
   * @constant
   */
  const name = '2';
}