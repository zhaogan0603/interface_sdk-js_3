/**
 * the ut for method in class, method is callback
 */
export class Test {
  test(param1: string, callback: AsyncCallback<Want>): void;
}