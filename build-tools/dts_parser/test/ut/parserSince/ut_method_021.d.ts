/**
 * the ut for method in class, which doesn't have params and return value
 */
export class Test {
  test(): void;
}