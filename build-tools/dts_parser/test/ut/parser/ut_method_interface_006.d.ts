/**
 * the ut for method in interface
 *
 */
export interface Test {
  (): void;
}
