import { ValueType } from 'exceljs';

type TextOneType = { [key: string]: ValueType | Uint8Array | null };
